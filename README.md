imotivate integration
=====================
imotivate integration module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist russkin/imotivate-integration "*"
```

or add

```
"russkin/imotivate-integration": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \russkin\imotivateintegration\AutoloadExample::widget(); ?>```